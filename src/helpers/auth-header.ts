export const authHeader = () => {
  // return authorization header with jwt token
  let user = this.$store.state.user.user;
  let token = this.$store.state.user.token;

  if (user && token) {
    return {'Authorization': 'Bearer ' + token};
  } else {
    return {};
  }
}
