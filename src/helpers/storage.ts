export const loadParam = (key: string) => {
  try {
    const serializedState = localStorage.getItem(key);
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (e) {
    return undefined;
  }
};

export const saveParam = (key: string, state: any) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem(key, serializedState);
  } catch (e) {
    //ignore errors
  }
};

export const removeParam = (key: string) => {
  try {
    return localStorage.removeItem(key);
  } catch (e) {
    return undefined;
  }
};
