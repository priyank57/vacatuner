import Vue from 'vue';
import {IonicVueRouter} from '@ionic/vue';
import {NavigationGuard} from "vue-router";
import {loadParam} from "@/helpers/storage";

Vue.use(IonicVueRouter);

const privateRoute: NavigationGuard = function (to, from, next) {

  const loggedIn = loadParam('authUser');
  if (!loggedIn) {
    next({name: 'login'});
  } else {
    next();
  }
};

// base: process.env.BASE_URL,
export default new IonicVueRouter({
  mode: 'history',
  base: '/',
  routes: [
    {
      path: '/login',
      name: 'login',
      component: () => import('@/views/Login.vue')
    },
    {
      path: '/home',
      name: 'home',
      component: () => import('@/views/Home.vue'),
      beforeEnter: privateRoute
    },
    {
      path: '/trips',
      name: 'trips',
      component: () => import('@/views/Trips.vue'),
      beforeEnter: privateRoute
    },
    {
      path: '/profile',
      name: 'profile',
      component: () => import('@/views/Profile.vue'),
      beforeEnter: privateRoute
    },
    {
      path: '/',
      redirect: 'home'
    }
  ]
});
