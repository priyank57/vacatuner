const trip = {
  state: {trips: null},
  mutations: {
    setTrip(state, trips) {
      state.trips = trips.data;
    }
  },
  actions: {
    async getTrips({commit}, loginForm) {

      const response = await fetch('/data/shifts.json')
      const trips = await response.json()

      commit('setTrip', trips);
    }
  }
};

export default trip;
