import router from './../../router';
import {loadParam, removeParam, saveParam} from "@/helpers/storage";
import {userService} from "@/services";

const user = {
  state: {authUser: null},
  mutations: {
    logIn(state, user) {
      state.authUser = user
    },
    logOut(state) {
      state.authUser = null
      removeParam('authUser')
    },
    loadAuthUser(state) {
      state.authUser = loadParam('authUser') ? loadParam('authUser') : null
    },
  },
  actions: {
    async logIn({commit}, loginForm) {
      // userService.login(loginForm.userName, loginForm.password).then();

      const response = await fetch('/data/auth/login.json')
      const user = await response.json()

      let authUser = user.user;
      authUser.token = user.token;
      saveParam('authUser', authUser)

      commit('logIn', authUser);
      router.push('/');
    },
    logOut({commit}) {
      commit('logOut');
      router.push('/login');
    },
    loadAuthUser({commit}) {
      commit('loadAuthUser');
    },
  }
};

export default user;
