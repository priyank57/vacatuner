import Vue from 'vue';
import Vuex from 'vuex';
import user from "@/store/modules/user";
import trip from "@/store/modules/trip";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    user,
    trip
  }
});
