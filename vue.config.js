module.exports = {
  lintOnSave: false,
  chainWebpack: config => {
    // config.plugins.delete('preload');
    //   config.plugins.delete('hmr');
    config.module
      .rule('vue')
      .use('vue-loader')
      .loader('vue-loader')
      .tap(options => {
        options.hotReload = false; // disables Hot Reload
        return options;
      });
  },
  css: {
    loaderOptions: {
      sass: {
        data: `
          @import "@/assets/scss/App.scss";
          @import "@/assets/scss/app/_variables.scss";
        `
      }
    }
  }
};
